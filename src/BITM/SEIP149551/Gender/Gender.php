<?php

namespace App\Gender;
use App\Message\Message;

use App\Utility\Utility;

use App\Model\Database as DB;

use PDO;

class Gender extends DB
{

    public $id;
    public $name;
    public $gender;


    public function __construct()
    {

        parent::__construct();

    }
    public function setData($postVariableData=NULL)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['$postVariableData'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('gender',$postVariableData))
        {
            $this->gender=$postVariableData['gender'];
        }
    }
    public  function  store()
    {
        $arrData=array($this->name,$this->gender);
        $sql="Insert INTO gender(name,gender) VALUE (?,?)";
        //var_dump($sql);

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('create.php');
    }//end of store m

    public function index()
    {

    }
}// end of BookTitle class