<?php

namespace App\SummaryOfOrganization;

use App\Model\Database as DB;
use App\Message\Message;

use App\Utility\Utility;


use PDO;

class SummaryOfOrganization extends DB
{

    public $id;
    public $orgname;
    public $summaryorg;


    public function __construct()
    {

        parent::__construct();

    }
    public function setData($postVariableData=NULL)
    {

        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['$postVariableData'];
        }
        if(array_key_exists('orgname',$postVariableData))
        {
            $this->orgname=$postVariableData['orgname'];
        }
        if(array_key_exists('summaryorg',$postVariableData))
        {
            $this->summaryorg=$postVariableData['summaryorg'];

        }
    }
    public  function  store()
    {
        $arrData=array($this->orgname,$this->summaryorg);
        $sql="Insert INTO summaryoforganization(orgname,summaryorg) VALUE (?,?)";
        //var_dump($sql);

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('create.php');
    }//end of store m
    public function index()
    {

    }
}// end of BookTitle class