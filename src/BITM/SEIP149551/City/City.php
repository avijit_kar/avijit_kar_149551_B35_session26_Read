<?php

namespace App\City;
use App\Message\Message;

use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class City extends DB
{

    public $id;
    public $name;
    public $city;


    public function __construct()
    {

        parent::__construct();

    }
    
    

        public function setData($postVariableData=NULL)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['$postVariableData'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('city',$postVariableData))
        {
            $this->city=$postVariableData['city'];
        }
    }
        public  function  store()
    {
        $arrData=array($this->name,$this->city);
        $sql="Insert INTO city(name,city) VALUE (?,?)";
        //var_dump($sql);

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('create.php');
    }//end of store m
        
}// end of BookTitle class